//
//  OpenCVWrapper.h
//  opencv ios app
//
//  Created by Arvin Jha on 6/18/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenCVWrapper : NSObject

//declare the opencv function and its signature
+ (NSString *)openCVVersionString;


@end

NS_ASSUME_NONNULL_END
