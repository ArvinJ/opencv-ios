//
//  OpenCVWrapper.m
//  opencv ios app
//
//  Created by Arvin Jha on 6/18/21.
//

#import <opencv2/opencv.hpp>
#import "OpenCVWrapper.h"

@implementation OpenCVWrapper

//function implementation from .h file
+ (NSString *)openCVVersionString {
    return [NSString stringWithFormat:@"OpenCV Version %s",  CV_VERSION];
}

@end
